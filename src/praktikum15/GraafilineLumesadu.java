package praktikum15;

import javafx.scene.canvas.GraphicsContext;

public class GraafilineLumesadu extends Lumesadu {

	public GraafilineLumesadu(int helvesteArv, GraphicsContext gc) {
		super(helvesteArv, (int) gc.getCanvas().getWidth(), (int) gc.getCanvas().getHeight());
		for (Lumehelves lumehelves : lumehelbed) {

			lumehelves.joonistaMind(gc);

		}

	}	

}


// gc.strokeArc(100, 200, 200, 100, 180, 180, ArcType.OPEN);