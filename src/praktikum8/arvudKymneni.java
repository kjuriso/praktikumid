package praktikum8;

import praktikum5.TextIO;

public class ArvudKymneni {

	public static void main(String[] args) {

		System.out.println("Palun sisesta kümme arvu.");

		int[] arvud = new int[10];

		for (int i = 0; i < arvud.length; i++) {

			arvud[i] = TextIO.getlnInt();

		}

		System.out.println("Sinu numbrid tagurpidi on:");

		for (int i = 9; i < arvud.length; i--) {
			System.out.println(arvud[i]);
		}

	}

}
