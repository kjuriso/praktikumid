package praktikum13;

import java.util.ArrayList;

public class Keskmine {

	public static void main(String[] args) {
		String kataloogitee = Keskmine.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailiLugeja.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);

		// TODO leida aritmeetiline keskmine
		double sum = 0;
		//double count = 0;
		double keskmine = 0;
		double vigaseidRidu = 0;
		for (String rida : failiSisu) {
			try {
			double nr = Double.parseDouble(rida);
			sum += nr;
			} catch (NumberFormatException e) {
				System.out.println("See ei ole number: " + rida);
				vigaseidRidu = vigaseidRidu + 1;//count = count - 1;
			}	
			//count = count + 1;

		}
		//keskmine = sum / count;
		
		keskmine = sum / (failiSisu.size() - vigaseidRidu);
		System.out.println(keskmine);
	}
}
