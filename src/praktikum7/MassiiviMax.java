package praktikum7;

public class MassiiviMax {

	public static void main(String[] args) {

		System.out.println(maksimum(new int[][] { { 11, 2, 3, 45 }, { 4, 7, 3 }, { 4, 3, 1 } }));
	}

	// meetod, mis leiab ühemõõtmelise massiivi maksimumi
	public static int maksimum(int[] massiiv) {
		int max = 0;

		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > max) {
				max = massiiv[i];
			}
		}
		return max;
	}

	// meetod, mis leiab maatriksi maksimumi
	public static int maksimum(int[][] maatriks) {
		int max = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maksimum(maatriks[i]) > max) {
					max = maksimum(maatriks[i]);
				}
			}
		}
		return max;
	}
}
