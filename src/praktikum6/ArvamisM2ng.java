package praktikum6;

import praktikum5.TextIO;

public class ArvamisM2ng {

	public static void main(String[] args) {

		int arvutiArv = suvalineArv(1, 100);

		while (true) {

			System.out.println("Mis number on?");

			int kasutajaArv = TextIO.getlnInt();

			if (arvutiArv == kasutajaArv) {

				System.out.println("Tubli! Arvasid �ra, õige arv on " + arvutiArv + ".");
				break;
			} else if (kasutajaArv > arvutiArv) {
				System.out.println("Arvuti valitud arv on v�iksem");
			} else {
				System.out.println("Arvuti valitud arv on suurem");
			}

		}

	}

	// teeme meetodi, mis tagastab suvalise arvu etteantud vahemikus

	public static int suvalineArv(int min, int max) {

		// math.random v�ljastab nr-i 0-st 1-ni

		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;

	}
}