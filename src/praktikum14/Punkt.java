package praktikum14;

public class Punkt {

	// extends Object vaikimisi olemas

	int x;
	int y;

	public Punkt(int x, int y) {
		this.x = x; // kui oleks i ja j, siis x=i ja y=j
		this.y = y;

		// konstruktormeetod on see meetod,
		// mis kutsutakse v�lja, kui me loome uue objekti
		// konstruktoreid v�ib olla koodis mitu t�kki, neid
		// valitakse v�lja argumendi v��rtuse j�rgi
	}

	public Punkt() {
		// Luuakse punkt, millel ei ole koordinaate m���ratud
	}
	@Override // annotatsioon, ei muuda programmi t��d,
	// aga need on vihjed Eclipsile,
	// kuidas me oleme programmi �les ehitanud
	// ja mida me tahame kindlasti teha
	public String toString() {
		// TODO tr�kkida v�lja punkt
		
		return "Punkt(x=" + x + " ,y=" + y + ")";
	}
}
