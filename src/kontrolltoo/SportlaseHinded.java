package kontrolltoo;

public class SportlaseHinded {

	public static void main(String[] args) {

		System.out.println(result(new double[] { 0.0, 6.0, 3.0, 9.0, 1.0, 4.0 }));

	}

	public static double result(double[] marks) {

		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		double sum = 0;
		double keskmine = 0;

		for (int i = 0; i < marks.length; i++) {
			sum = sum + marks[i];
		}

		for (int i = 0; i < marks.length - 1; i++) {
			if (i == 0) {
				min = Math.min(marks[i], marks[i + 1]);
				max = Math.max(marks[i], marks[i + 1]);
			} else {
				min = Math.min(min, marks[i + 1]);
				max = Math.max(max, marks[i + 1]);
			}
		}

		sum = sum - min - max;

		keskmine = sum / (marks.length - 2);

		return keskmine;
	}

}
