package praktikum10;

public class rekursioon {

	public static void main(String[] args) {

		int i = 0;
		while (true) {
			System.out.println(i + " - " + fibonacci(i));
			i++;
		}

		// System.out.println(astenda(2, 8));

	}

	public static long fibonacci(int n) {

		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else
			return fibonacci(n - 1) + fibonacci(n - 2);

	}

	public static int astenda(int arv, int aste) {
		// astenda(2, 3) = 2 * astenda(2, 2) = 2 * 2 * astenda(2, 1) = 2 * 2 * 2

		if (aste == 1)
			return arv;
		else
			return arv * astenda(arv, aste - 1);

	}

}
