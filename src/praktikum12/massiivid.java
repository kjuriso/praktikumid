package praktikum12;

public class massiivid {

	public static void main(String[] args) {

		int[] massiiv = { 4, 6, 6, 1, 3, 5, 6, 7, 5, 8 };
		tryki(massiiv);

		int[][] neo = { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 }, { 3, 3, 3, 3, 3 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };
		//int[][] another = {{1,2},{3,4},{5,6}};
		
		tryki(neo);
		tryki(ridadeSummad(neo));
		System.out.println(korvalDiagonaaliSumma(neo));
		tryki(ridadeMaksimumid(neo));
		System.out.println(miinimum(neo));
		tryki(kahegaJaakMaatriks(3, 5));
		tryki(transponeeri(neo));

	}

	public static void tryki(int[] massiiv) {

		for (int i = 0; i < massiiv.length; i++)
			System.out.print(massiiv[i] + " ");
	}

	public static void tryki(int[][] maatriks) {
		System.out.println();
		System.out.println();
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]); // kutsub v�lja juba kirjutatud meetodi
			System.out.println();
		}
	}

	public static int[] ridadeSummad(int[][] maatriks) {
		System.out.println();
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {

			summad[i] = reaSumma(maatriks[i]);

		}
		return summad;
	}

	public static int reaSumma(int[] rida) {
		int summa = 0;
		for (int i : rida) {
			summa += i;
		}
		return summa;
	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		System.out.println();
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				if (i + j == maatriks.length - 1) {
					summa = summa + maatriks[i][j];
				}
			}
		}
		return summa;
	}

	public static int[] ridadeMaksimumid(int[][] maatriks) {
		System.out.println();

		int[] maxid = new int[maatriks.length];

		for (int i = 0; i < maatriks.length; i++) {
			int max = Integer.MIN_VALUE;

			for (int el : maatriks[i]) {
				if (el > max) {
					max = el;
				}
			}
			maxid[i] = max;
		}
		return maxid;
	}

	public static int miinimum(int[][] maatriks) {
		System.out.println();

		int min = Integer.MAX_VALUE;

		for (int[] rida : maatriks) {
			for (int el : rida) {
				if (el < min) {
					min = el;
				}
			}
		}
		return min;

	}

	// Kirjutada programm, mis genereerib parameetritena
	// etteantud suurusega maatriksi, kus iga element on
	// rea ja veeruindeksi summa kahega jagamise j��k. Indeksid algavad nullist.

	public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
		System.out.println();

		int[][] temp = new int[ridu][veerge];

		for (int i = 0; i < ridu; i++) {
			for (int j = 0; j < veerge; j++) {
				temp[i][j] = (i + j) % 2;

			}
		}
		return temp;
	}
	
	public static int[][] transponeeri(int[][] maatriks){
		System.out.println();
		
		int[][] maatriks2 = new int[maatriks.length][maatriks[0].length];
		
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				maatriks2[j][i] = maatriks[i][j];
			}
		}
		
		return maatriks2;
		
	}
}
