package praktikum9;

public class Rekursioon {

    public static void main(String[] args){

        System.out.println(astenda(5,2));
    }

    public static int astenda(int arv, int aste){
        if (aste != 0){
            return (int) Math.pow(arv, aste);
        }else{
            return arv;
        }
        
    }
}

