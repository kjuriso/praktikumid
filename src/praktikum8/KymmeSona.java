package praktikum8;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class KymmeSona {

	public static Scanner userInput = new Scanner(System.in);
	
	public static void main(String[] args) {
		ArrayList<String> nimedeList = new ArrayList<String>();
		
		System.out.println("Palun sisesta k�mme nime:");
		
		while (nimedeList.size() < 10) {
			System.out.print((nimedeList.size() + 1) + ": ");
			nimedeList.add(getName());
		}
		
		System.out.println("\nNimede pikkused ja nimed: ");
		for (String element : nimedeList){
			System.out.println(element.length()+ " " + element);
		}
		userInput.close();
	}
	
	private static String getName(){
		String nimi = " ";
		try{
			nimi = userInput.next();
		}
		catch (InputMismatchException e){}
		return nimi;
		}
	}

