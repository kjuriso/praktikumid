package praktikum8;

public class inimeseN2ited {

	public static void main(String[] args) {

		@SuppressWarnings("unused")
		String nimi = new String("Mati");

		Inimene keegi = new Inimene("Kati", 34); // uus objekt, kutsub välja
													// konstruktori
		System.out.println(keegi.vanus);

		keegi.tervita();

		Inimene keegiVeel = new Inimene("Mati", 30);

		keegiVeel.tervita();

		System.out.println(keegi);
	}
}
