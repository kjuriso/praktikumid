package praktikum2;

public class Grupid {

	public static void main(String[] args) {

		// 7 jagada 3-ga on 2 ja jääk 1
		// 7 % 3 => 1 ehk annab kohe jäägi

		int arv;
		System.out.println("Palun sisesta inimeste arv:");
		arv = TextIO.getlnInt();

		int grupp;
		System.out.println("Palun sisesta grupi suurus:");
		grupp = TextIO.getlnInt();

		int jagatis = (int) Math.floor(arv / grupp);
		int jääk = arv % grupp;

		System.out.println("Neist saab moodustada " + jagatis + " gruppi ja �le j��b " + jääk + " inimest.");
	}

}