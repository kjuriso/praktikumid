package praktikum8;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class NimedJaVanused {
	
	public static Scanner userInput = new Scanner(System.in);
	
	public static void main(String[] args) {
	ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
	int nimedeArv = 0;
	
	System.out.println("Mitu nime soovid sisestada?");
	nimedeArv = getAge();
	
	while (inimesed.size() < nimedeArv){
		
		System.out.print("Palun sisesta nimi:");
		String nimi = getName();
		
		System.out.print("Palun sisesta vanus:");
		int vanus = getAge();
					
		inimesed.add(new Inimene(nimi, vanus));
	}

	for (Inimene inimene : inimesed) {
	    // Java kutsub v�lja Inimene klassi toString() meetodi
	   
	   inimene.tervita();
	}
}

	private static String getName() {
		 String name = "";
	        try {
	            name = userInput.next();
	        }catch (InputMismatchException e){}
	        return name;
	    }

	private static int getAge() {    
	int age = 0;
    try {
        age = userInput.nextInt();
    }catch (InputMismatchException e){
        System.out.println("ERROR: " + e);
        userInput.next();
    }
    return age;
}
}
