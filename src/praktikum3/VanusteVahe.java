package praktikum3;

public class VanusteVahe {

	public static void main(String[] args) {

		System.out.println("Palun sisesta kaks vanust:");
		int vanus1 = TextIO.getlnInt();
		int vanus2 = TextIO.getlnInt();

		int vanusevahe = Math.abs(vanus1 - vanus2);

		if (vanusevahe > 5) {
			if (vanusevahe > 10)
				System.out.println("V�ga kahtlane!");
			else
				System.out.println("Kahtlane!");
		} else {
			System.out.println("Nii on ok!");
		}

	}
}
